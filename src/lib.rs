use async_trait::async_trait;
use chrono::NaiveDateTime;
use figment::{
    providers::{Data, Format, Serialized, Toml},
    util::map,
    value::Map,
    Figment,
};
use gethostname::gethostname;
use influxdb2::models::DataPoint;
use serde_derive::{Deserialize, Serialize};
use std::{path::Path, time::Duration};
use tokio::sync::broadcast::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct InfluxConfig {
    pub url: String,
    pub org: String,
    pub bucket: String,
    pub token: String,
    pub tags: Map<String, String>,
}

impl Default for InfluxConfig {
    fn default() -> Self {
        let host = gethostname().into_string().unwrap();
        InfluxConfig {
            url: "http://localhost:8086".into(),
            org: "default".into(),
            bucket: "default".into(),
            token: "".into(),
            tags: map! { "host".into() => host },
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ChronyConfig {
    pub enabled: bool,
    pub timeout: u64,
    pub tracking_interval: u64,
    pub sources_interval: u64,
    pub measurement_prefix: String,
    pub tracking_measurement: String,
    pub sources_measurement: String,
    pub host: String,
}

impl Default for ChronyConfig {
    fn default() -> Self {
        ChronyConfig {
            enabled: false,
            timeout: 5,
            tracking_interval: 60,
            sources_interval: 300,
            measurement_prefix: "chrony.".into(),
            tracking_measurement: "tracking".into(),
            sources_measurement: "sources".into(),
            host: "127.0.0.1:323".into(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ChronySockConfig {
    pub enabled: bool,
    pub sock: String,
}

impl Default for ChronySockConfig {
    fn default() -> Self {
        ChronySockConfig {
            enabled: false,
            sock: "".into(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct HwmonSensorConfig {
    pub name: String,
    pub sensor: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct HwmonConfig {
    pub enabled: bool,
    pub interval: u64,
    pub measurement: String,
    pub sensors: Map<String, HwmonSensorConfig>,
}

impl Default for HwmonConfig {
    fn default() -> Self {
        HwmonConfig {
            enabled: false,
            interval: 60,
            measurement: "hwmon".into(),
            sensors: map! {},
        }
    }
}
#[derive(Clone, Debug)]
pub struct TimeReport {
    pub system_time: NaiveDateTime,
    pub offset: chrono::Duration,
    pub leaps: isize,
    pub leap_flag: bool,
    pub valid: bool,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct UCCMConfig {
    pub enabled: bool,
    pub port: String,
    pub baud: u32,
    pub status_interval: std::time::Duration,
    pub timeout: std::time::Duration,
    pub measurement: String,
}

impl Default for UCCMConfig {
    fn default() -> Self {
        UCCMConfig {
            enabled: false,
            port: "/dev/ttyS0".into(),
            baud: 57600,
            status_interval: std::time::Duration::from_secs(10),
            timeout: std::time::Duration::from_secs(1),
            measurement: "uccm_gpsdo".into(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SourcesConfig {
    pub chrony: ChronyConfig,
    pub hwmon: HwmonConfig,
    pub uccm: UCCMConfig,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct TargetsConfig {
    pub chrony: ChronySockConfig,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Config {
    pub influxdb: InfluxConfig,
    pub sources: SourcesConfig,
    pub targets: TargetsConfig,
}

pub fn load_config(filename: &Path) -> Figment {
    Figment::from(Serialized::defaults(Config::default())).merge(Toml::file(filename))
}

#[derive(Clone, Debug)]
pub enum ChimemonMessage {
    DataPoint(DataPoint),
    DataPoints(Vec<DataPoint>),
    TimeReport(TimeReport),
}

impl From<DataPoint> for ChimemonMessage {
    fn from(dp: DataPoint) -> Self {
        ChimemonMessage::DataPoint(dp)
    }
}
impl From<Vec<DataPoint>> for ChimemonMessage {
    fn from(dps: Vec<DataPoint>) -> Self {
        ChimemonMessage::DataPoints(dps)
    }
}

impl From<TimeReport> for ChimemonMessage {
    fn from(tr: TimeReport) -> Self {
        ChimemonMessage::TimeReport(tr)
    }
}

pub type ChimemonSourceChannel = Sender<ChimemonMessage>;
pub type ChimemonTargetChannel = Receiver<ChimemonMessage>;

#[async_trait]
pub trait ChimemonSource {
    async fn run(self, chan: ChimemonSourceChannel);
}

#[async_trait]
pub trait ChimemonTarget {
    async fn run(self, chan: ChimemonTargetChannel);
}
