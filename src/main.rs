mod chrony;
mod chrony_refclock;
mod hwmon;
mod uccm;

use chrono::NaiveDateTime;
use clap::{Parser, ValueEnum};
use env_logger::{self, Env};
use futures::{future::join_all, prelude::*};
use log::{debug, error, info, warn};
use std::path::Path;
use tokio::sync::broadcast;

use crate::{chrony::*, chrony_refclock::ChronySockServer, hwmon::HwmonSource, uccm::UCCMMonitor};
use chimemon::*;

const PROGRAM_NAME: &str = "chimemon";
const VERSION: &str = "0.0.1";

#[derive(ValueEnum, Clone)]
enum Level {
    Debug,
    Info,
    Warn,
    Error,
}

#[derive(Parser)]
struct Args {
    /// TOML configuration to load
    #[arg(short, long, default_value_t = String::from("config.toml"))]
    config_file: String,
    #[arg(value_enum, default_value_t = Level::Warn)]
    log_level: Level,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    let loglevel = args
        .log_level
        .to_possible_value()
        .unwrap()
        .get_name()
        .to_owned();
    env_logger::Builder::from_env(Env::default().default_filter_or(loglevel)).init();

    info!("{} v{} starting...", PROGRAM_NAME, VERSION);
    let fig = load_config(Path::new(&args.config_file));
    debug!("{:?}", fig);
    let config: Config = fig.extract()?;

    let mut tasks = Vec::new();
    let (tx, _) = broadcast::channel(16);
    let sourcechan: ChimemonSourceChannel = tx;

    info!(
        "Connecting to influxdb {} org: {} using token",
        &config.influxdb.url, &config.influxdb.org
    );
    let influx = influxdb2::Client::new(
        &config.influxdb.url,
        &config.influxdb.org,
        &config.influxdb.token,
    );

    let chrony = if config.sources.chrony.enabled {
        Some(ChronyClient::new(config.to_owned()))
    } else {
        None
    };
    match chrony {
        Some(c) => {
            tasks.push(tokio::spawn(c.run(sourcechan.clone())));
        }
        None => (),
    };

    let hwmon = if config.sources.hwmon.enabled {
        Some(HwmonSource::new(config.to_owned()))
    } else {
        None
    };
    match hwmon {
        Some(hwmon) => {
            tasks.push(tokio::spawn(hwmon.run(sourcechan.clone())));
        }
        None => (),
    };

    let uccm = if config.sources.uccm.enabled {
        info!("Spawning UCCMMonitor");
        Some(UCCMMonitor::new(config.to_owned()))
    } else {
        info!("UCCMMonitor not configured");
        None
    };
    match uccm {
        Some(uccm) => {
            tasks.push(tokio::spawn(uccm.run(sourcechan.clone())));
        }
        None => (),
    };

    let chrony_refclock = if config.targets.chrony.enabled {
        Some(ChronySockServer::new(config.targets.chrony.to_owned()))
    } else {
        None
    };
    match chrony_refclock {
        Some(chrony_refclock) => {
            tasks.push(tokio::spawn(chrony_refclock.run(sourcechan.subscribe())));
        }
        None => (),
    };

    let mut influxrx = sourcechan.subscribe();
    tasks.push(tokio::spawn(async move {
        loop {
            match influxrx.recv().await {
                Ok(msg) => match msg {
                    ChimemonMessage::DataPoint(dp) => {
                        debug!("Writing datapoint to influx: {:?}", dp);

                        influx
                            .write(&config.influxdb.bucket, stream::iter([dp]))
                            .await
                            .unwrap_or_else(|e| error!("Error writing to influxdb {:?}", e));
                    }
                    ChimemonMessage::DataPoints(dps) => {
                        debug!("Writing datapoints to influx: {:?}", dps);

                        influx
                            .write(&config.influxdb.bucket, stream::iter(dps))
                            .await
                            .unwrap_or_else(|e| error!("Error writing to influxdb {:?}", e));
                    }
                    ChimemonMessage::TimeReport(tr) => {
                        debug!("GPS TOD: {:?}", tr);
                    }
                },
                Err(e) => error!("Unable to receive from channel: {:?}", e),
            }
        }
    }));

    // let mut debugrx = sourcechan.subscribe();
    // tasks.push(tokio::spawn(async move {
    //     loop {
    //         let v = debugrx.recv().await;
    //         warn!("streamed: {:?}", v.unwrap());
    //     }
    // }));

    join_all(tasks).await;

    Ok(())
}
