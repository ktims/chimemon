use async_trait::async_trait;
use chimemon::{ChimemonSource, ChimemonSourceChannel, Config};
use futures::{stream, StreamExt};
use influxdb2::models::DataPoint;
use log::{debug, info};
use std::{
    path::PathBuf,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

pub struct HwmonSource {
    config: Config,
    sensors: Vec<HwmonSensor>,
}

struct HwmonSensor {
    path: PathBuf,
    device: String,
    sensor: String,
    name: String,
}

const HWMON_ROOT: &str = "/sys/class/hwmon";

impl HwmonSource {
    pub fn new(config: Config) -> Self {
        let sensors = config
            .sources
            .hwmon
            .sensors
            .iter()
            .map(|(k, v)| HwmonSensor {
                name: k.into(),
                device: v.name.clone(),
                sensor: v.sensor.clone(),
                path: PathBuf::from(HWMON_ROOT).join(&v.name).join(&v.sensor),
            })
            .collect();

        HwmonSource { config, sensors }
    }

    async fn get_raw_value(sensor: &HwmonSensor) -> Result<String, std::io::Error> {
        tokio::fs::read_to_string(&sensor.path).await
    }
}

#[async_trait]
impl ChimemonSource for HwmonSource {
    async fn run(self, chan: ChimemonSourceChannel) {
        info!("hwmon task started");
        let mut interval =
            tokio::time::interval(Duration::from_secs(self.config.sources.hwmon.interval));
        loop {
            interval.tick().await;
            let s = stream::iter(&self.sensors).then(|s| async {
                let sensor_val = HwmonSource::get_raw_value(s)
                    .await
                    .expect("Unable to read sensor");
                debug!(
                    "hwmon {} raw value {}",
                    s.path.to_string_lossy(),
                    sensor_val
                );
                let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
                let mut builder =
                    DataPoint::builder(&self.config.sources.hwmon.measurement).timestamp(now.as_nanos().try_into().unwrap());
                for (key, value) in &self.config.influxdb.tags {
                    builder = builder.tag(key, value)
                }
                builder = builder
                    .tag("sensor", &s.name)
                    .field("value", sensor_val.trim().parse::<i64>().unwrap());
                builder.build().unwrap()
            });
            info!("Writing hwmon data");
            chan.send(s.collect::<Vec<DataPoint>>().await.into())
                .unwrap();
        }
    }
}
