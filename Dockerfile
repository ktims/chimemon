FROM rust:slim as builder

RUN apt-get update && apt-get install -y libssl-dev pkg-config
# build deps only first for build cache
WORKDIR /usr/src
RUN USER=root cargo new chimemon
COPY Cargo.toml Cargo.lock /usr/src/chimemon/
WORKDIR /usr/src/chimemon
RUN cargo build --release
COPY . .
RUN cargo build --release

FROM debian:bullseye-slim
WORKDIR /app
COPY --from=builder /usr/src/chimemon/target/release/chimemon chimemon
CMD ["/app/chimemon"]
